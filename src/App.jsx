import { useEffect, useState } from "react";
import axios from "axios"; //On importe la bibliothèque axios qui permet d'effectuer des requêtes HTTP
import "./App.css";
import TodoList from "./components/TodoList";

const API_URL = "https://jsonplaceholder.typicode.com/todos";

function App() {
  const [todos, setTodos] = useState([]);

  // TODO install and import axios, create an useEffect hook, and call the API
  useEffect(() => { //On utilise le hook useEffect, qui prend en argument: la fonction à exécuter et un tableau de dépendances.
    axios
      .get(API_URL) //On effectue une requête HTTP sur l'url indiquée
      .then((response) => { //Lorsqu'on reçoit la réponse
        setTodos(response.data) //setTodos met à jour l'état du composant 
      })
  }, [])

  return (
    <div className="App">
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
